
public class Gato {
	
	String color;
	String raza;
	String sexo;
	int edad;
	double peso;

	// Constructores
	
	Gato (String s){
		this.sexo = s;
	}
	Gato (){
		
	}
	
	Gato (String color, String raza, String sexo, int edad, double peso){
		this.color = color;
		this.raza = raza;
		this.sexo = sexo;
		this.edad = edad;
		this.peso = peso;
	}
	
	void maullar () {
		System.out.println("Miauuuuuu");
	}
	
	void ronronear () {
		System.out.println("mrrrrr");
	}
	
	void comer (String comida) {
		if (comida.equals("pescado")) {
			System.out.println("Hmmmm, gracias");
		}else {
			System.out.println("No me gusta esa comida");
		}
	}
	
	void pelearCon(Gato contrincante) {
		if (this.sexo.equals("hembra")) {
			System.out.println("No me gusta pelear");
		}else {
			if(contrincante.sexo.equals("hembra")) {
				System.out.println("No peleo con gatitas");
			}else {
				System.out.println("Ven aqu� que te vas a enterar");
			}
		}
		
	}
}
