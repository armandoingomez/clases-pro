package primitiva;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		primitiva();

	}
	private static void primitiva() {

        Scanner lectorTeclado = new Scanner(System.in);
        ArrayList <Integer>listaNumeros = new ArrayList<>(5);
        ArrayList<Integer> listaGenerados = new ArrayList<>(5);
        
        int numero, x=0, cont=0, cont2=0, aciertos=0, premio=10;

        System.out.printf("Introduce 5 numeros para jugar: %n");

        for (int i = 0; i < 5; i++) {

            System.out.printf("Introduce el numero %d: ",(i+1));
            numero = lectorTeclado.nextInt();
            listaNumeros.add(numero);

            if (numero<0 || numero>99){
                i--;
                System.out.println("Número incorrecto, introduzca otro");
            }

        }

        System.out.println("Tu boleto: ");
        for (int num :
                listaNumeros) {
            cont++;
            System.out.println(cont+": "+num);
        }

        for (int i = 0; i < 5; i++) {

            int numGenerado = (int)(Math.random()*49);
            listaGenerados.add(numGenerado) ;
        }

        System.out.println("El boleto del sistema: ");
        for (int num2 :
                listaGenerados) {
            cont2++;
            System.out.println(cont2+": "+num2);
        }

        for (int i = 0; i < 5; i++) {

            if (listaGenerados.get(i)==listaNumeros.get(i)){
                aciertos++;
            }

        }

        switch (aciertos){

            case 0:
                premio=0;
                break;
            case 1:
                premio=10;
                break;
            case 2:
                premio=100;
                break;
            case 3:
                premio=1000;
                break;
            case 4:
                premio=10000;
                break;
            case 5:
                premio=100000;
                break;

        }

        System.out.printf("Has tenido "+aciertos+ " aciertos y has ganado "+ premio);

    }


}
