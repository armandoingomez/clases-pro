package parte2;

import java.util.Date;

public class Persona {
	String nombre;
	String apellido;
	String dni;
	Date fecha;
	
	// Constructor con todos los campos
	public Persona(String nombre, String apellido, String dni, Date fecha) {
		
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
		this.fecha = fecha;
	}
	// Constructor con el campo nombre
	public Persona(String nombre) {
		
		this.nombre = nombre;
	}

	
	
}
