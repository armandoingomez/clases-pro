package parte2;

public class Cuenta {
	Persona titular;
	String banco;
	double saldo;

	// Constructor con todos los campos
	public Cuenta(Persona titular, String banco, double saldo) {
		
		this.titular = titular;
		this.banco = banco;
		this.saldo = saldo;
	}

	void ingresarDinero(double cantidad) {
		saldo = saldo + cantidad;

		System.out.println("Ha ingresado " + cantidad);
	}

	void sacarDinero(double cantidad) {
		if (saldo >= cantidad) {
			saldo = saldo - cantidad;
			System.out.println("Ha retirado " + cantidad);
		} else {
			System.out.println("No dispone de tanta cantidad");
		}

	}

	void mostrarSaldo() {
		System.out.println("El saldo de su cuenta es " + saldo);

	}

	void cambiarTitular(Persona titular) {
		this.titular = titular;
		System.out.println("Titular cambiado");

	}

	void mostrarDatosTitular() {
		System.out.println("Nombre: " + this.titular.nombre);
		System.out.println("Apellidos: " + this.titular.apellido);
		System.out.println("Dni: " + this.titular.dni);
		System.out.println("Fecha: " + this.titular.fecha);
	}

}
