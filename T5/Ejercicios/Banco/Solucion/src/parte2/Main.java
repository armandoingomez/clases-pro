package parte2;

import java.sql.Date;

public class Main {

	public static void main(String[] args) {
		
		/* Para pasarle un Date al objeto  utilizamos el m�todo valueOf() de la clase java.sql.Date, 
		 * que toma una cadena en el formato "yyyy-MM-dd" como argumento y devuelve un objeto Date.*/
		Persona p = new Persona("Juan","Sanz","22222",Date.valueOf("1990-02-02"));
		Persona p2 = new Persona("Maria","Garcia","766545",Date.valueOf("1983-09-06"));
		Persona p3 = new Persona("Roberto","Jimenez","689322",Date.valueOf("1973-12-05"));
		Cuenta c1 = new Cuenta(p,"Santander",100);
		Cuenta c2 = new Cuenta(p2, "BBVA",100);
		
		c1.ingresarDinero(200);
		c2.ingresarDinero(200);
		c1.sacarDinero(30);
		c1.mostrarSaldo();
		c2.mostrarSaldo();
		c1.cambiarTitular(p3);
		c1.mostrarDatosTitular();

		c1.ingresarDinero(200);
		c2.ingresarDinero(200);
		c1.sacarDinero(30);
		c1.mostrarSaldo();
		c2.mostrarSaldo();


	}

}
