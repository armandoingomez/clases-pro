package parte1;

public class Cuenta {
	String titular;
	String banco;
	double saldo;

	// Constructor con todos campos
	public Cuenta(String titular, String banco, double saldo) {

		this.titular = titular;
		this.banco = banco;
		this.saldo = saldo;
	}

	// Constructor con el campo titular
	public Cuenta(String titular) {
		
		this.titular = titular;
	}

	void ingresarDinero(double cantidad) {
		this.saldo = this.saldo + cantidad;

		System.out.println("Ha ingresado " + cantidad);
	}

	void sacarDinero(double cantidad) {
		if (this.saldo >= cantidad) {
			this.saldo = this.saldo - cantidad;
			System.out.println("Ha retirado " + cantidad);
		} else {
			System.out.println("No dispone de tanta cantidad");
		}

	}

	void mostrarSaldo() {
		System.out.println("El saldo de su cuenta es " + this.saldo);

	}

}
